package com.gbournac.question.theme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThemeEnabler {

	public static void main(String[] args) {
		SpringApplication.run(ThemeEnabler.class, args);
	}
}
