package com.gbournac.question.theme.api.implementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

import com.gbournac.question.common.services.logger.LogFormater;
import com.gbournac.question.theme.domain.ThemeController;

/**
 * The theme controller implementation
 * 
 * @author Guénaël Bournac
 *
 */
@RestController
public class ThemeControllerImplementation implements ThemeController {

	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(ThemeControllerImplementation.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHome() {
		String sMethodName = "getHome";
		LogFormater.info(log, sMethodName, "someone get the home");
		return "Home";
	}
}
