#!/bin/bash

if [[ $# -ne 10 ]]; then
    echo "Usage ./script.sh {project_name} {IMAGE_NAME} {maven_group_id} {maven_version} {maven_parent_version} {maven_parent_repository} {maven_common_version} {maven_common_repository} {ci_project} {ci_file}"
    exit 2
fi

project_name=$1
image_name=$2
maven_group_id=$3
maven_version=$4
maven_parent_version=$5
maven_parent_repository=$6
maven_common_version=$7
maven_common_repository=$8
ci_project=$9
ci_file=${10}

echo $project_name
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#PROJECT_NAME~$project_name~g" {} +
echo $image_name
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#IMAGE_NAME~$image_name~g" {} +
echo $maven_group_id
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_GROUP_ID~$maven_group_id~g" {} +
echo $maven_version
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_VERSION~$maven_version~g" {} +
echo $maven_parent_version
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_PARENT_VERSION~$maven_parent_version~g" {} +
echo $maven_parent_repository
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_PARENT_REPOSITORY_URL~$maven_parent_repository~g" {} +
echo $maven_common_version
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_COMMON_VERSION~$maven_common_version~g" {} +
echo $maven_common_repository
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#MAVEN_COMMON_REPOSITORY_URL~$maven_common_repository~g" {} +
echo $ci_project
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#CI_PROJECT~$ci_project~g" {} +
echo $ci_file
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#CI_FILE~$ci_file~g" {} +

mv domain $project_name-domain
mv report $project_name-report
mv services $project_name-services

#IMAGE_NAME = theme
#CI_FILE = .gitlab_ci_cd_java.yml
