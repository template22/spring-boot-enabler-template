package com.gbournac.question.theme.domain;

import org.springframework.web.bind.annotation.GetMapping;

/**
 * The controller used for the theme question management
 * 
 * @author Guénaël Bournac
 */
public interface ThemeController {

	/**
	 * A function for test purpose
	 * 
	 * @return
	 */
	@GetMapping("/home")
	public String getHome();

}
